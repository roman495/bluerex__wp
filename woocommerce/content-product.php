<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
};

global $product;
?>
<div class="col">
    <div id="product1" class="card product">
        <?php do_action( 'woocommerce_before_shop_loop_item' ) ?>
            
        <?php do_action( 'woocommerce_before_shop_loop_item_title' ) ?>
                
        <?php do_action( 'woocommerce_shop_loop_item_title' ) ?>
            
        <?php do_action( 'woocommerce_after_shop_loop_item_title' ) ?>
        
        <?php do_action( 'woocommerce_after_shop_loop_item' ) ?>
    </div>
</div>
