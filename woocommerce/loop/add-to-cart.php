<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
};

global $product;

$classList = [];
$classList[] = 'btn';
$classList[] = 'btn-violet';
$classList[] = 'rounded-pill';

if( isset( $class ) ) {
    foreach( explode( ' ', $class ) as $classItem ) {
        $classList[] = $classItem;
    }
}

if( ( $key = array_search( 'button', $classList ) ) !== false ) {
    unset( $classList[$key] );
}

$classList = implode( ' ', $classList ); 
$product_sku = esc_attr( $product->get_sku() );
?>
    <div class="d-flex justify-content-between align-items-center mb-3">
        <span class="code">Code: <?= $product_sku ?></span>
        <a 
            href="<?= esc_url( $product->add_to_cart_url() ) ?>" 
            data-quantity="<?= esc_attr( isset( $quantity ) ? $quantity : 1 ) ?>"
            data-product_id="<?= esc_attr( $product->id ) ?>"
            data-product_sku="<?= $product_sku ?>"
            data-price="<?= esc_attr( $product->get_price_html() ) ?>"
            class="<?= $classList ?>"
        >
            Buy
        </a>    
    </div>
</div>
<!-- /.card-body -->