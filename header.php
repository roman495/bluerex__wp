<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
    
	<link rel="apple-touch-icon" sizes="180x180" href="<?= get_template_directory_uri() ?>/assets/img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= get_template_directory_uri() ?>/assets/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= get_template_directory_uri() ?>/assets/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?= get_template_directory_uri() ?>/assets/img/favicon/site.webmanifest">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

    <button id="scrollToTop" class="scroll-to-top">
        <i class="fas fa-angle-up"></i>
    </button>

    <header
    <?php if ( is_front_page() ) : ?>
        class="main-header" 
        style="background: url(<?= has_post_thumbnail() ? get_the_post_thumbnail_url() : get_template_directory_uri() . '/assets/img/main1.jpg' ?>) center no-repeat;"
    <?php else : ?>
        class="page-header"
    <?php endif ?>
    >
        <nav class="navbar navbar-expand-lg pt-0">
            <div class="container-fluid <?= ( ! is_front_page() ) ? ' navbar-dark bg-dark' : '' ?>">
                <a class="navbar-brand" href="<?= home_url( '/' ) ?>">
                    <?php
                        $custom_logo = wp_get_attachment_image_src(
                            get_theme_mod( 'custom_logo' )
                        );
                        if ( $custom_logo ) : $custom_logo_src = $custom_logo[0];
                    ?>
                        <img src="<?= $custom_logo_src ?>" alt="<?= bloginfo( 'name' ) ?>">
                    <?php endif ?>
                    <?= bloginfo( 'name' ) ?>
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fas fa-bars"></i>
                </button>
                <div id="navbarSupportedContent" class="collapse navbar-collapse justify-content-md-end">
                    <?php wp_nav_menu(
                        array(
                            'theme_location' => 'menu-1',
                            'menu_id'        => 'primary-menu',
                            'container_class'=> '',
                            'container_id'   => '',
                            'menu_class'     => 'navbar-nav me-auto',
                            'walker'         => new Bluerex_Header_Menu,
                        )
                    );?>
                    <?php if( ! is_front_page() ) : ?>
                        <ul class="navbar-nav ms-auto">
                            <?php if( is_user_logged_in() ) : ?>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?= wp_logout_url( home_url() ); ?>">
                                        <i class="fas fa-sign-out-alt"></i>
                                    </a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="shopCart" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                        <i class="fas fa-shopping-cart"></i> (<span class="total-count-label">0</span>)
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-end shopping-cart-desc p-3" aria-labelledby="shopCart">
                                        Total count: <span class="total-count">0</span><br>
                                        Total cost: $<span class="total-cost">0.00</span><br>
                                        <div class="d-grid gap-2">
                                            <a class="btn btn-violet m-0 mt-2 p-1" href="<?= get_page_link( 7 ) ?>">View cart</a>
                                        </div>
                                    </div>
                                </li>
                            <?php else : ?>
                                <li class="nav-item">
                                    <a id="signUpBtn" class="nav-link"href="#!">
                                        <i class="fas fa-user-plus"></i>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a id="signInBtn" class="nav-link" href="#!">
                                        <i class="fas fa-sign-in-alt"></i>
                                    </a>
                                </li>
                            <?php endif ?>
                        </ul>
                    <?php endif ?>
                </div>
            </div>
        </nav>

        <?php if ( is_front_page() ) : ?>
            <div class="main-header-text">
                <h3><?= get_post_meta( $post->ID, 'title', true ) ?></h3>
                <h4><?= get_post_meta( $post->ID, 'subtitle', true ) ?></h4>
                <p><?= get_post_meta( $post->ID, 'description', true ) ?></p>
                <div class="main-header-buttons">
                    <a href="<?= get_post_meta( $post->ID, 'link_href_1', true ) ?>" class="btn rounded-pill btn-pink"><?= get_post_meta( $post->ID, 'link_title_1', true ) ?></a>
                    <a href="<?= get_post_meta( $post->ID, 'link_href_2', true ) ?>" class="btn rounded-pill btn-violet"><?= get_post_meta( $post->ID, 'link_title_2', true ) ?></a>
                </div>
            </div>
        <?php endif ?>
    </header>
