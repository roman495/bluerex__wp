<?php

function add_orders_posts_columns( $columns ) {
    $new_columns = [
        'name'   => __( 'Имя клиента', 'bluerex' ),
        'status' => __( 'Статус заявки', 'bluerex' ),
    ];
    return array_slice( $columns, 0, 1 ) + $new_columns + $columns;
}
add_filter( 'manage_orders_posts_columns', 'add_orders_posts_columns' );

function render_orders_posts_columns( $column_name, $post_ID ) {
    if( $column_name == 'name' ) {
        ?>
            <a style="text-transform:capitalize" href="<?php echo get_edit_post_link() ?>">
                <?php echo get_post_meta( $post_ID, $column_name, true ) ?>
            </a>
        <?php
    } elseif( $column_name == 'status' ) {
        $status_list = [
            'new'        => __( 'Новая заявка', 'bluerex' ),
            'done'       => __( 'Заявка обработана', 'bluerex' ),
            'processing' => __( 'Требуется уточнение', 'bluerex' ),
        ];
        $status_key = get_post_meta( $post_ID, $column_name, true );
        $status_value = $status_list[ $status_key ];
        ?>
            <a href="<?php echo get_edit_post_link() ?>">
                <?php echo $status_value ?>
            </a>
        <?php
    }
    return $column_name;
}
add_filter( 'manage_orders_posts_custom_column', 'render_orders_posts_columns', 10, 2 );
