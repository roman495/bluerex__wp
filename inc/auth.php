<?php

add_action( 'wp_ajax_nopriv_registration', 'bx_registration' );
add_action( 'wp_ajax_nopriv_auth', 'bx_auth' );

function bx_registration() {
    $login = $_POST['bxSignUpLogin'] ?? '';
    $email = $_POST['bxSignUpEmail'] ?? '';
    $pass  = $_POST['bxSignUpPassword'] ?? '';
    $conf  = $_POST['bxSignUpConfirmPassword'] ?? '';
    $nonce = $_POST['nonce'] ?? '';

    $login = sanitize_text_field( $login );
    $email = sanitize_text_field( $email );
    $pass = sanitize_text_field( $pass );
    $conf = sanitize_text_field( $conf );

    if( ! wp_verify_nonce( $nonce, 'bluerex_nonce' ) ) {
        wp_die( 'Ошибка запроса!' );
    }
    
    if( empty( $login ) ) {
        wp_die( 'Заполните login' );
    }

    if( empty( $email ) ) {
        wp_die( 'Заполните email' );
    }

    if( empty( $pass ) ) {
        wp_die( 'Заполните password' );
    }

    if( empty( $conf ) ) {
        wp_die( 'Заполните confirm password' );
    }

    if( $pass !== $conf ) {
        wp_die( 'Пароль и подтверждение не совпадают!' );
    }

    $res = wp_create_user( $login, $pass, $email );

    if( ! is_wp_error( $res ) ) {
        wp_die( $res, 200 );
    }
    wp_die( 'Ошибка регистрации' );
}

function bx_auth() {
    $login = $_POST['bxSignInLogin'] ?? '';
    $pass  = $_POST['bxSignInPassword'] ?? '';
    $nonce = $_POST['nonce'] ?? '';

    $login = sanitize_text_field( $login );
    $pass = sanitize_text_field( $pass );

    if( ! wp_verify_nonce( $nonce, 'bluerex_nonce' ) ) {
        wp_die( 'Ошибка запроса!' );
    }
    
    if( empty( $login ) ) {
        wp_die( 'Заполните login', 400 );
    }

    if( empty( $pass ) ) {
        wp_die( 'Заполните password', 400 );
    }

    $user = wp_authenticate( $login, $pass );

    if( is_wp_error( $user ) ) {
        wp_die( 'Ошибка аутентификации', 400 );
    }

    wp_set_auth_cookie( $user->ID );
    wp_die( get_userdata( $user->ID )->user_login, 200 );
}
