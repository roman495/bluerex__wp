<?php

add_filter( 'manage_portfolio_posts_columns', function ( $columns ) {
    $new_columns = [
        'image' => __( 'Миниатюра', 'bluerex' ),
        'type' => __( 'Типы работ', 'bluerex' ),
    ];
    return array_slice( $columns, 0, 1 ) + $new_columns + $columns;
} );

add_filter( 'manage_portfolio_posts_custom_column', function ( $column_name, $post_ID ) {
    if( $column_name == 'image' && has_post_thumbnail() ) {
        ?>
            <a href="<?php echo get_edit_post_link() ?>">
                <?php the_post_thumbnail( 'thumbnail' ) ?>
            </a>
        <?php
    } elseif ( $column_name == 'type' ) {
        the_taxonomies();
    }
    return $column_name;
}, 10, 2 );

add_filter( 'manage_testimonials_posts_columns', function ( $columns ) {
    $new_columns = [
        'client' => __( 'Автор отзыва', 'bluerex' ),
    ];
    return array_slice( $columns, 0, 1 ) + $new_columns + $columns;
} );

add_filter( 'manage_testimonials_posts_custom_column', function ( $column_name, $post_ID ) {
    if( $column_name == 'client' ) {
        ?>
            <a href="<?php echo get_edit_post_link() ?>">
                <?php echo get_post_meta( $post_ID, $column_name, true ) ?>
            </a>
        <?php
    }
    return $column_name;
}, 10, 2 );
