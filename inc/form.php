<?php

// add_action( 'admin_post_nopriv_bluerex_form', 'bluerex_form_handler' );
// add_action( 'admin_post_bluerex_form', 'bluerex_form_handler' );

add_action( 'wp_ajax_nopriv_bluerex_form', 'bluerex_form_handler' );
add_action( 'wp_ajax_bluerex_form', 'bluerex_form_handler' );

function bluerex_form_handler() {
    $name  = $_POST['bluerex_name'] ?? 'Anonimous';
    $email = $_POST['bluerex_email'] ?? false;

    if ( ! $email ) {
        header('Location:', home_url() );
    }

    $name = sanitize_text_field( $name );
    $email = sanitize_text_field( $email );

    $id = wp_insert_post([
        'post_title'  => 'Заявка # ',
        'post_type'   => 'orders',
        'post_status' => 'publish',
        'meta_input'  => [
            'name'   => $name,
            'email'  => $email,
            'status' => 'new',
        ],
    ] );

    if ( $id ) {
        wp_update_post( [
            'ID'         => $id,
            'post_title' => 'Заявка # ' . $id,
        ] );
    }
    echo 'Ваша заявка # ' . $id . ' принята!';
    wp_die();
}
