<?php

add_action( 'admin_init', 'bx_register_socials' );

function bx_register_socials() {

    $fields = [
        'facebook',
        'twitter',
        'youtube',
        'instagram',
        'linkedin',
        'phone',
    ];

    foreach( $fields as $field ) {
        add_settings_field(
            'bx_opiton_field_' . $field,
            ucfirst( $field ),
            'bx_option_social_cb',
            'general',
            'default',
            ['label_for' => 'bx_opiton_field_' . $field]
        );
        
        register_setting(
            'general',
            'bx_opiton_field_' . $field,
            'strval',
        );
    }
}

function bx_option_social_cb( $args ) {
?>
    <input
        type="text"
        class="regular-text"
        id="<?php echo $args['label_for'] ?>"
        name="<?php echo $args['label_for'] ?>"
        value="<?php echo get_option( $args['label_for'] ) ?>"
    >
<?php
}
