<?php if ( $featured_query->have_posts() ) : ?>
    <div class="owl-carousel owl-theme owl-loaded">
        <?php while ( $featured_query->have_posts() ) : $featured_query->the_post(); ?>
            <a href="<?= get_the_permalink() ?>">
                <img style="max-width:150px;height:auto;" class="img-thumbnail" src="<?= get_the_post_thumbnail_url( get_the_ID(), 'full' ) ?>" alt="">
            </a>
        <?php endwhile ?>
        <?php wp_reset_postdata() ?>
    </div>
<?php endif ?>
