<?php

class Bluerex_Product_Favorites_Widget extends WP_Widget
{
    public function __construct()
    {
        parent::__construct(
            'Bluerex_Product_Favorites_Widget',
            __( 'Избранные продукты', 'bluerex' ),
            [
                'description' => __( 'Выводит избранные продукты', 'bluerex' ),
                'classname'   => 'product-favorites_widget',
            ]
        );
    }
    
    public function widget( $args, $instance )
    {
        extract( $instance );
        $pcount = $pcount ?? 0;

        $featured_query = new WP_Query( [
            'post_type'      => 'product',
            // 'meta_key'       => '_featured',
            // 'meta_value'     => 'yes',
            'posts_per_page' => $pcount,
            'orderby'        => ['ID' => 'DESC'],
        ] );
        include 'template.php';
    }

    public function form( $instance )
    {
        extract( $instance );
        $pcount = isset( $pcount ) ? $pcount : 0;
        ?>
        <p>
            <label for="<?= $this->get_field_id('pcount') ?>"><?php _e( 'Количество записей', 'bluerex' ) ?>:</label>
            <input 
                type="number" 
                name="<?= $this->get_field_name('pcount') ?>" 
                id="<?= $this->get_field_id('pcount') ?>" 
                class="widefat title" 
                value="<?php echo esc_attr( $pcount ) ?>"
            >
        </p>
        <?php
    }
    
    public function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance[ 'pcount' ] = strip_tags( $new_instance[ 'pcount' ] );
        return $instance;
    }
}