<?php
require get_template_directory() . '/inc/widgets/product-favorites/class-bx-widget-product-favorites.php';

add_action( 'widgets_init', 'bx_product_bottom_widgets_init' );
add_action( 'wp_enqueue_scripts', 'bx_product_bottom_scripts' );

function bx_product_bottom_widgets_init() {

	register_sidebar(
		array(
			'name'          => esc_html__( 'Content bottom', 'bluerex' ),
			'id'            => 'product-bottom',
			'description'   => esc_html__( 'Блок для отображения слайдера товара', 'bluerex' ),
			'before_widget' => '',
			'after_widget'  => '',
			'before_title'  => '',
			'after_title'   => '',
		)
	);

    register_widget( 'Bluerex_Product_Favorites_Widget' );
}

function bx_product_bottom_scripts() {
	wp_deregister_script('jquery-core');
	wp_deregister_script('jquery');

	// регистрируем
	wp_register_script( 'jquery-core', 'https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js', false, null, true );
	wp_register_script( 'jquery', false, array('jquery-core'), null, true );

	// подключаем
	wp_enqueue_script( 'jquery' );
		
	wp_enqueue_style( 
		'bx-owl', 
		get_template_directory_uri() . '/inc/widgets/product-favorites/assets/css/owl.carousel.min.css' 
	);
	
	wp_enqueue_style( 
		'bx-owl-theme', 
		get_template_directory_uri() . '/inc/widgets/product-favorites/assets/css/owl.theme.default.min.css' 
	);

	wp_enqueue_script( 
		'bx-owl', 
		get_template_directory_uri() . '/inc/widgets/product-favorites/assets/js/owl.carousel.min.js', 
		['jquery'], 
		'', 
		true 
	);

	wp_enqueue_script( 
		'bx-owl-settings', 
		get_template_directory_uri() . '/inc/widgets/product-favorites/assets/js/script.js', 
		['bx-owl'], 
		'', 
		true 
	);
}