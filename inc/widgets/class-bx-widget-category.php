<?php

class Bluerex_Category_Widget extends WP_Widget
{
    public function __construct()
    {
        parent::__construct(
            'Bluerex_Category_Widget',
            __( 'Категории (аккардион)', 'bluerex' ),
            [
                'description' => __( 'Вывод категорий в виде аккардиона', 'bluerex' ),
                'classname'   => 'widget-categories',
            ]
        );
    }
    
    public function widget( $args, $instance )
    {
        extract( $args );
        extract( $instance );

        $title = isset( $title ) ? $title : '';
        $exlude = isset( $exlude ) ? $exlude : '';

        $title = apply_filters( 'widget_title', $title );

        $cats = wp_list_categories([
            'title_li' => '',
            'echo'     => false,
            'exclude'  => $exlude,
        ]);

        echo $args['before_widget'];
        echo $args['before_title'] . $title . $args['after_title'];
        ?>
        <ul class="list-group">
            <?php echo $cats ?>
        </ul>    
        <?php
        echo $args['after_widget'];
    }

    public function form( $instance )
    {
        extract( $instance );
        $title = isset( $title ) ? $title : '';
        $exlude = isset( $exlude ) ? $exlude : '';
        ?>
        <p>
            <label for="<?= $this->get_field_id('title') ?>"><?php _e( 'Заголовок', 'bluerex' ) ?>:</label>
            <input 
                type="text" 
                name="<?= $this->get_field_name('title') ?>" 
                id="<?= $this->get_field_id('title') ?>" 
                class="widefat title" 
                value="<?php echo esc_attr( $title ) ?>"
            >
        </p>
        <p>
            <label for="<?= $this->get_field_id('exlude') ?>"><?php _e( 'ID исключенных рубрик через запятую', 'bluerex' ) ?>:</label>
            <input 
                type="text" 
                name="<?= $this->get_field_name('exlude') ?>" 
                id="<?= $this->get_field_id('exlude') ?>" 
                class="widefat" 
                value="<?php echo esc_attr( $exlude ) ?>"
            >
        </p>
        <?php
    }
}