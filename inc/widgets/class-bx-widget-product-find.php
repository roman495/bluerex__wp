<?php

class Bluerex_Product_Find_Widget extends WP_Widget
{
    public function __construct()
    {
        parent::__construct(
            'Bluerex_Product_Find_Widget',
            __( 'Поиск продуктов', 'bluerex' ),
            [
                'description' => __( 'Вывод формы поиска продуктов', 'bluerex' ),
                'classname'   => 'product-find_widget',
            ]
        );
    }
    
    public function widget( $args, $instance )
    {
        // extract( $args );
        // extract( $instance );

        // $title = isset( $title ) ? $title : '';
        // $exlude = isset( $exlude ) ? $exlude : '';

        // $title = apply_filters( 'widget_title', $title );

        // $cats = wp_list_categories([
        //     'title_li' => '',
        //     'echo' => false,
        //     'exclude' => $exlude,
        // ]);

        ?>
        <div id="findProduct" class="card collapse find-products">
            <div class="card-header text-white bg-violet">Find products</div>
            <form action="<?= home_url('/') ?>">
                <div class="card-body">
                    <div class="input-group mb-3">
                        <input 
                            type="text" 
                            class="form-control" 
                            name="s"
                            value="<?=get_search_query()?>" 
                            placeholder="Search query...  " 
                            aria-label="Recipient's username" 
                            aria-describedby="button-addon2"
                        >
                        <button class="btn btn-violet" type="submit" id="button-addon2">Search</button>
                    </div>

                    <!-- <div class="more-options text-center">
                        <a class="btn btn-violet text-decoration-none" data-bs-toggle="collapse" href="#searchOptions" role="button" aria-expanded="false" aria-controls="collapseExample">
                            More filters <i class="fas fa-caret-down"></i>
                        </a>
                    </div> -->
                </div>
                <!-- <div id="searchOptions" class="collapse">
                    <h6 class="card-header text-white bg-violet mt-3">Category filter</h6>
                    <div id="categoryOption" class="list-group">
                        <label class="list-group-item">
                            <input class="form-check-input me-1 category-all" name="cat" type="checkbox" value="1">
                            All
                        </label>
                        <label class="list-group-item">
                            <input class="form-check-input me-1 category-item" name="cat" type="checkbox" value="2">
                            E-book
                        </label>
                        <label class="list-group-item">
                            <input class="form-check-input me-1 category-item" name="cat" type="checkbox" value="3">
                            Notebook
                        </label>
                        <label class="list-group-item">
                            <input class="form-check-input me-1 category-item" name="cat" type="checkbox" value="4">
                            IPhone
                        </label>
                        <label class="list-group-item">
                            <input class="form-check-input me-1 category-item" name="cat" type="checkbox" value="5">
                            Android
                        </label>
                    </div>
                    <h6 class="card-header text-white bg-violet mt-3">Producers filter</h6>
                    <div id="producerOption" class="list-group">
                        <label class="list-group-item">
                            <input class="form-check-input me-1 producer-all" name="prod" type="checkbox" value="1">
                            All
                        </label>
                        <label class="list-group-item">
                            <input class="form-check-input me-1 producer-item" name="prod" type="checkbox" value="2">
                            Apple
                        </label>
                        <label class="list-group-item">
                            <input class="form-check-input me-1 producer-item" name="prod" type="checkbox" value="3">
                            Samsung
                        </label>
                        <label class="list-group-item">
                            <input class="form-check-input me-1 producer-item" name="prod" type="checkbox" value="4">
                            Huawei
                        </label>
                        <label class="list-group-item">
                            <input class="form-check-input me-1 producer-item" name="prod" type="checkbox" value="5">
                            Intel
                        </label>
                    </div>
                </div> -->
            </form>
        </div>
        <?php
    }

    public function form( $instance )
    {
        extract( $instance );
        $title = isset( $title ) ? $title : '';
        $exlude = isset( $exlude ) ? $exlude : '';
        ?>
        <p>
            <label for="<?= $this->get_field_id('title') ?>"><?php _e( 'Заголовок', 'bluerex' ) ?>:</label>
            <input 
                type="text" 
                name="<?= $this->get_field_name('title') ?>" 
                id="<?= $this->get_field_id('title') ?>" 
                class="widefat title" 
                value="<?php echo esc_attr( $title ) ?>"
            >
        </p>
        <p>
            <label for="<?= $this->get_field_id('exlude') ?>"><?php _e( 'ID исключенных рубрик через запятую', 'bluerex' ) ?>:</label>
            <input 
                type="text" 
                name="<?= $this->get_field_name('exlude') ?>" 
                id="<?= $this->get_field_id('exlude') ?>" 
                class="widefat" 
                value="<?php echo esc_attr( $exlude ) ?>"
            >
        </p>
        <?php
    }
}