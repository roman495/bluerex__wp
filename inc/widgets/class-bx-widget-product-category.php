<?php

class Bluerex_Product_Category_Widget extends WP_Widget
{
    public function __construct()
    {
        parent::__construct(
            'Bluerex_Product_Category_Widget',
            __( 'Категории продуктов', 'bluerex' ),
            [
                'description' => __( 'Вывод категорий продуктов', 'bluerex' ),
                'classname'   => 'product-category_widget',
            ]
        );
    }
    
    public function widget( $args, $instance )
    {
        ?>
        <div id="productCatelog" class="card mt-3 collapse categories">
            <div class="card-header text-white bg-violet">Product catalog</div>
            <div class="list-group">
                <a href="#" class="list-group-item list-group-item-action d-flex justify-content-between align-items-center" aria-current="true">
                    Smartphone
                    <span class="badge bg-violet rounded-pill">5</span>
                </a>
                <a href="#" class="list-group-item list-group-item-action d-flex justify-content-between align-items-center" aria-current="true">
                    Computer
                    <span class="badge bg-violet rounded-pill">10</span>
                </a>
                <a href="#" class="list-group-item list-group-item-action d-flex justify-content-between align-items-center" aria-current="true">
                    Notebook
                    <span class="badge bg-violet rounded-pill">14</span>
                </a>
            </div>
        </div>
        <?php
    }

    public function form( $instance )
    {
        extract( $instance );
        $title = isset( $title ) ? $title : '';
        $exlude = isset( $exlude ) ? $exlude : '';
        ?>
        <p>
            <label for="<?= $this->get_field_id('title') ?>"><?php _e( 'Заголовок', 'bluerex' ) ?>:</label>
            <input 
                type="text" 
                name="<?= $this->get_field_name('title') ?>" 
                id="<?= $this->get_field_id('title') ?>" 
                class="widefat title" 
                value="<?php echo esc_attr( $title ) ?>"
            >
        </p>
        <p>
            <label for="<?= $this->get_field_id('exlude') ?>"><?php _e( 'ID исключенных рубрик через запятую', 'bluerex' ) ?>:</label>
            <input 
                type="text" 
                name="<?= $this->get_field_name('exlude') ?>" 
                id="<?= $this->get_field_id('exlude') ?>" 
                class="widefat" 
                value="<?php echo esc_attr( $exlude ) ?>"
            >
        </p>
        <?php
    }
}