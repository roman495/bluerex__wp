<?php

class BluerexGallery_Widget extends WP_Widget
{
    public function __construct()
    {
        parent::__construct(
            'BluerexGallery_Widget',
            __( 'Моя галерея', 'bluerex' ),
            array(
                'classname'   => 'bx_gallery_widget',
                'description' => __( 'Виджет галереи для футера сайта', 'bluerex' )
            )
        );    
    }
    
    public function widget( $args, $instance )
    {
        ?>
        <div class="row gallery_widget">
            <?php for($i = 1; $i <= 4; $i++ ) : ?>
                <div class="col-6">
                    <a href="<?php echo wp_get_attachment_image_url( (int) $instance['photo_' . $i], 'large' ) ?>">
                        <img 
                            src="<?php echo wp_get_attachment_image_url( (int) $instance['photo_' . $i], 'medium' ) ?>" 
                            alt=""
                        >
                    </a>
                </div>
            <?php endfor ?>
        </div>
        <?php
    }

    public function form( $instance )
    {
        ?>
        <?php for($i = 1; $i <= 4; $i++ ) : ?>
            <p>
                <label for="<?= $this->get_field_id( 'photo_' . $i ) ?>"><?php _e( 'Фото ID ' . $i, 'law' ) ?>:</label>
                <input 
                    type="text" 
                    name="<?= $this->get_field_name( 'photo_' . $i ) ?>" 
                    id="<?= $this->get_field_id( 'photo_' . $i ) ?>" 
                    class="widefat" 
                    value="<?php echo esc_attr( $instance['photo_' . $i] ) ?>"
                >
            </p>
        <?php endfor ?>
        <?php
    }
}