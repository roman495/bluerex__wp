<?php

remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );

add_action( 'woocommerce_after_shop_loop_item_title', 'show_product_short_description' );

add_filter( 'woocommerce_sale_flash', 'change_sale_flash' );

function woocommerce_template_loop_product_thumbnail() {
    echo woocommerce_get_product_thumbnail();
}

function woocommerce_template_loop_product_title() {
?>
<div class="card-body">
    <h5 class="card-title"><?= get_the_title() ?></h5>
<?php
}

function show_product_short_description() {
?>
    <span class="price badge">$100.00</span>
<?php
}

function change_sale_flash() {
    return '<span class="sale badge">SALE</span>';
}
