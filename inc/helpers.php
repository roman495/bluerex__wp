<?php

function dd($data)
{
    echo '<pre class="m-3 p-3 rounded bg-dark text-white text-left font-monospace fs-6">' . print_r( $data, 1 ) . '</pre>';
}
