<?php

function create_taxonomy()
{
	register_taxonomy( 'worktype', [ 'services', 'portfolio' ], [
		'label'                 => '',
		'labels'                => [
			'name'              => __( 'Типы работ', 'bluerex' ),
			'singular_name'     => __( 'Тип работы', 'bluerex' ),
			'search_items'      => __( 'Поиск типов работ', 'bluerex' ),
			'all_items'         => __( 'Все типы работ', 'bluerex' ),
			'view_item '        => __( 'Просмотреть тип работы', 'bluerex' ),
			'parent_item'       => __( 'Parent Genre', 'bluerex' ),
			'parent_item_colon' => __( 'Parent Genre:', 'bluerex' ),
			'edit_item'         => __( 'Редактировать тип работы', 'bluerex' ),
			'update_item'       => __( 'Обновить тип работы', 'bluerex' ),
			'add_new_item'      => __( 'Добавить тип работы', 'bluerex' ),
			'new_item_name'     => __( 'Новый тип работы', 'bluerex' ),
			'menu_name'         => __( 'Типы работы', 'bluerex' ),
		],
		'description'           => '', // описание таксономии
		'public'                => true,
		// 'publicly_queryable'    => null, // равен аргументу public
		// 'show_in_nav_menus'     => true, // равен аргументу public
		// 'show_ui'               => true, // равен аргументу public
		// 'show_in_menu'          => true, // равен аргументу show_ui
		// 'show_tagcloud'         => true, // равен аргументу show_ui
		// 'show_in_quick_edit'    => null, // равен аргументу show_ui
		'hierarchical'          => false,

		'rewrite'               => true,
		//'query_var'             => $taxonomy, // название параметра запроса
		'capabilities'          => array(),
		'meta_box_cb'           => null, // html метабокса. callback: `post_categories_meta_box` или `post_tags_meta_box`. false — метабокс отключен.
		'show_admin_column'     => false, // авто-создание колонки таксы в таблице ассоциированного типа записи. (с версии 3.5)
		'show_in_rest'          => null, // добавить в REST API
		'rest_base'             => null, // $taxonomy
		// '_builtin'              => false,
		//'update_count_callback' => '_update_post_term_count',
	] );
}
add_action( 'init', 'create_taxonomy' );
