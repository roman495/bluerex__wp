<?php
function custom_post_types() {
    register_post_type( 'services', [
        'labels' => [
            'name'               => __( 'Услуги', 'bluerex' ),
            'singular_name'      => __( 'Услуга', 'bluerex' ),
            'add_new'            => __( 'Добавить новую услугу', 'bluerex' ),
            'add_new_item'       => __( 'Добавить новую услугу', 'bluerex' ),
            'edit_item'          => __( 'Редактировать услугу', 'bluerex' ),
            'new_item'           => __( 'Новая услуга', 'bluerex' ),
            'view_item'          => __( 'Посмотреть услугу', 'bluerex' ),
            'search_items'       => __( 'Найти услугу', 'bluerex' ),
            'not_found'          => __( 'Услуг не найдено', 'bluerex' ),
            'not_found_in_trash' => __( 'В корзине услуг не найдено', 'bluerex' ),
            'parent_item_colon'  => __( '', 'bluerex' ),
            'menu_name'          => __( 'Услуги', 'bluerex' ),
        ],
        'public'        => true,
        'has_archive'   => true,
        'rewrite'       => ['slug'],
        'menu_icon'     => 'dashicons-images-alt',
        // 'show_in_menu'  => 'sections',
        'show_in_menu'  => true,
        'supports'      => [
            'title',
            'editor',
            'thumbnail',
            'excerpt',
            // 'custom-fields',
        ],
    ] );

    register_post_type( 'progress', [
        'labels' => [
            'name'               => __( 'Прогресс', 'bluerex' ),
            'singular_name'      => __( 'Прогресс', 'bluerex' ),
            'add_new'            => __( 'Добавить новый прогресс', 'bluerex' ),
            'add_new_item'       => __( 'Добавить новый прогресс', 'bluerex' ),
            'edit_item'          => __( 'Редактировать прогресс', 'bluerex' ),
            'new_item'           => __( 'Новая прогресс', 'bluerex' ),
            'view_item'          => __( 'Посмотреть прогресс', 'bluerex' ),
            'search_items'       => __( 'Найти прогресс', 'bluerex' ),
            'not_found'          => __( 'Прогресс не найден', 'bluerex' ),
            'not_found_in_trash' => __( 'В корзине прогресс не найден', 'bluerex' ),
            'parent_item_colon'  => __( '', 'bluerex' ),
            'menu_name'          => __( 'Прогресс', 'bluerex' ),
        ],
        'public'        => true,
        'has_archive'   => true,
        'rewrite'       => ['slug'],
        'menu_icon'     => 'dashicons-editor-alignleft',
        // 'show_in_menu'  => 'sections',
        'show_in_menu'  => true,
        'supports'      => [
            'title',
            'editor',
            // 'thumbnail',
            // 'custom-fields',
        ],
    ] );

    register_post_type( 'portfolio', [
        'labels' => [
            'name'               => __( 'Портфолио', 'bluerex' ),
            'singular_name'      => __( 'Портфолио', 'bluerex' ),
            'add_new'            => __( 'Добавить новую работу', 'bluerex' ),
            'add_new_item'       => __( 'Добавить новую работу', 'bluerex' ),
            'edit_item'          => __( 'Редактировать работу', 'bluerex' ),
            'new_item'           => __( 'Новая работа', 'bluerex' ),
            'view_item'          => __( 'Посмотреть работу', 'bluerex' ),
            'search_items'       => __( 'Найти работу', 'bluerex' ),
            'not_found'          => __( 'Работа не найдена', 'bluerex' ),
            'not_found_in_trash' => __( 'В корзине работа не найдена', 'bluerex' ),
            'parent_item_colon'  => __( '', 'bluerex' ),
            'menu_name'          => __( 'Портфолио', 'bluerex' ),
        ],
        'public'        => true,
        'has_archive'   => true,
        'rewrite'       => ['slug'],
        'menu_icon'     => 'dashicons-format-gallery',
        'show_in_menu'  => true,
        'supports'      => [
            'title',
            'thumbnail',
        ],
    ] );

    register_post_type( 'orders', [
        'labels' => [
            'name'               => __( 'Заявки', 'bluerex' ),
            'singular_name'      => __( 'Заявки', 'bluerex' ),
            'add_new'            => __( 'Добавить новую заявку', 'bluerex' ),
            'add_new_item'       => __( 'Добавить новую заявку', 'bluerex' ),
            'edit_item'          => __( 'Редактировать заявку', 'bluerex' ),
            'new_item'           => __( 'Новая заявку', 'bluerex' ),
            'view_item'          => __( 'Посмотреть заявку', 'bluerex' ),
            'search_items'       => __( 'Найти заявку', 'bluerex' ),
            'not_found'          => __( 'Заявка не найдена', 'bluerex' ),
            'not_found_in_trash' => __( 'В корзине заявка не найдена', 'bluerex' ),
            'parent_item_colon'  => __( '', 'bluerex' ),
            'menu_name'          => __( 'Заявки', 'bluerex' ),
        ],
        'public'        => false,
        'show_ui'       => true,
        'show_in_menu'  => true,
        'has_archive'   => true,
        'rewrite'       => ['slug'],
        'menu_icon'     => 'dashicons-email',
        'show_in_menu'  => true,
        'show_in_rest'  => true,
        'supports'      => [
            'title',
        ],
    ] );

    register_post_type( 'testimonials', [
        'labels' => [
            'name'               => __( 'Отзывы', 'bluerex' ),
            'singular_name'      => __( 'Отзывы', 'bluerex' ),
            'add_new'            => __( 'Добавить новый отзыв', 'bluerex' ),
            'add_new_item'       => __( 'Добавить новый отзыв', 'bluerex' ),
            'edit_item'          => __( 'Редактировать отзыв', 'bluerex' ),
            'new_item'           => __( 'Новый отзыв', 'bluerex' ),
            'view_item'          => __( 'Посмотреть отзыв', 'bluerex' ),
            'search_items'       => __( 'Найти отзыв', 'bluerex' ),
            'not_found'          => __( 'Отзыв не найдена', 'bluerex' ),
            'not_found_in_trash' => __( 'В корзине отзыв не найдена', 'bluerex' ),
            'parent_item_colon'  => __( '', 'bluerex' ),
            'menu_name'          => __( 'Отзывы', 'bluerex' ),
        ],
        'public'        => true,
        'has_archive'   => true,
        'rewrite'       => ['slug'],
        'menu_icon'     => 'dashicons-format-status',
        'show_in_menu'  => true,
        'show_in_rest'  => true,
        'supports'      => [
            'title',
            'excerpt',
            'thumbnail',
        ],
    ] );
}
