<?php
/**
 * bluerex Theme Customizer
 *
 * @package bluerex
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function bluerex_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial(
			'blogname',
			array(
				'selector'        => '.site-title a',
				'render_callback' => 'bluerex_customize_partial_blogname',
			)
		);
		$wp_customize->selective_refresh->add_partial(
			'blogdescription',
			array(
				'selector'        => '.site-description',
				'render_callback' => 'bluerex_customize_partial_blogdescription',
			)
		);
	}

	// Section Footer
	$wp_customize->add_section(
		'bx_footer',
		[
			'title'			=> __( 'Футер сайта', 'bluerex' ),
			'description'	=> __( 'Заполнение копирайта в футере сайта', 'bluerex' ),
			'priority'		=> 200,
		]
	);

	$setting = 'bxFooterCopyright';

	$wp_customize->add_setting(
		$setting,
		[
			'default'	=> '© Copyright ' . date("Y"),
			'transport'	=> 'postMessage'
		]
	);

	$wp_customize->add_control(
		$setting,
		[
			'section'	=> 'bx_footer',
			'label'		=> __( 'Копирайт', 'bluerex' ),
			'type'		=> 'text'
		]
	);

	$setting = 'bxFooterPrivacy';

	$wp_customize->add_setting(
		$setting,
		[
			'default'	=> '#',
			'transport'	=> 'postMessage'
		]
	);

	$wp_customize->add_control(
		$setting,
		[
			'section'	=> 'bx_footer',
			'label'		=> __( 'Политика конфиденциальности', 'bluerex' ),
			'type'		=> 'text'
		]
	);

	$setting = 'bxFooterTerm';

	$wp_customize->add_setting(
		$setting,
		[
			'default'	=> '#',
			'transport'	=> 'postMessage'
		]
	);

	$wp_customize->add_control(
		$setting,
		[
			'section'	=> 'bx_footer',
			'label'		=> __( 'Условия использования', 'bluerex' ),
			'type'		=> 'text'
		]
	);

	$setting = 'bxFooterSitemap';

	$wp_customize->add_setting(
		$setting,
		[
			'default'	=> '#',
			'transport'	=> 'postMessage'
		]
	);

	$wp_customize->add_control(
		$setting,
		[
			'section'	=> 'bx_footer',
			'label'		=> __( 'Карта сайта', 'bluerex' ),
			'type'		=> 'text'
		]
	);
	
}
add_action( 'customize_register', 'bluerex_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function bluerex_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function bluerex_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function bluerex_customize_preview_js() {
	wp_enqueue_script( 'bluerex-customizer', get_template_directory_uri() . '/assets/js/customizer.js', array( 'customize-preview' ), _S_VERSION, true );
}
add_action( 'customize_preview_init', 'bluerex_customize_preview_js' );
