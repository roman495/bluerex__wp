<?php

function services_fields() {
	add_meta_box( 'services_fields', 'Дополнительные поля', 'services_fields_box_func', 'services', 'normal', 'high'  );
}
add_action('add_meta_boxes', 'services_fields', 1);

function services_fields_box_func( $post )
{
	?>
	<p>
        <label><b><?php _e( 'Класс иконок fontawesome (icon)', 'bluerex' ) ?></b></label>
        <input
            type="text" 
            name="services[icon]" 
            value="<?php echo get_post_meta( $post->ID, 'icon', 1 ); ?>"
            style="width:100%"
        >
    </p>
    <input
        type="hidden" 
        name="services_fields_nonce" 
        value="<?php echo wp_create_nonce(__FILE__); ?>"
    >
	<?php
}

function services_fields_update( $post_id )
{
	// базовая проверка
	if (
		   empty( $_POST['services'] )
		|| ! wp_verify_nonce( $_POST['services_fields_nonce'], __FILE__ )
		|| wp_is_post_autosave( $post_id )
		|| wp_is_post_revision( $post_id )
	)
		return false;

	// Все ОК! Теперь, нужно сохранить/удалить данные
	$_POST['services'] = array_map( 'sanitize_text_field', $_POST['services'] ); // чистим все данные от пробелов по краям
	foreach( $_POST['services'] as $key => $value ){
		if( empty($value) ){
			delete_post_meta( $post_id, $key ); // удаляем поле если значение пустое
			continue;
		}

		update_post_meta( $post_id, $key, $value ); // add_post_meta() работает автоматически
	}
 
	return $post_id;
}
add_action( 'save_post', 'services_fields_update', 0 );
