<?php

new Testimonials_Metabox;

class Testimonials_Metabox
{
    public $post_type = 'testimonials';

    public static $meta_key = 'testimonials';

    public function __construct()
    {
        add_action( 'add_meta_boxes', [ $this, 'add_metabox' ] );
        add_action( 'save_post', [ $this, 'save_metabox' ] );
    }

    public function add_metabox() 
    {
        add_meta_box( 
            'testimonials_fields', 
            'Дополнительные поля', 
            [ $this, 'render_metabox' ], 
            $this->post_type, 
            'normal', 
            'low'  
        );
    }

    public function render_metabox( $post ) 
    {
		?>
		<table class="form-table">
			<tr>
				<th scope="row">
                    <label for="<?php echo self::$meta_key ?>_client"><?php _e( 'Автор (client)', 'bluerex' ) ?></label>
				</th>
				<td>
                    <td>
                        <input 
                            name="<?php echo self::$meta_key ?>[client]" 
                            type="text" 
                            id="<?php echo self::$meta_key ?>_client" 
                            value="<?php echo get_post_meta( $post->ID, 'client', 1 ); ?>" 
                            placeholder="<?php _e( 'Автор', 'bluerex' ) ?>" 
                            class="regular-text"
                        ><br />
                        <span class="description"><?php _e( 'Автор отзыва', 'bluerex' ) ?></span>
                    </td>
				</td>
			</tr>
		</table>
		<?php
	}

    public function save_metabox( $post_id ) 
    {
		if ( wp_is_post_autosave( $post_id ) )
			return;

        if ( isset( $_POST[self::$meta_key] ) && is_array( $_POST[self::$meta_key] ) ) {
            $_POST[self::$meta_key] = array_map( 'sanitize_text_field', $_POST[self::$meta_key] );
        
            foreach( $_POST[self::$meta_key] as $key => $value ){
                if( empty($value) ){
                    delete_post_meta( $post_id, $key );
                    continue;
                }
                update_post_meta( $post_id, $key, $value );
            }    
        }
	}
}