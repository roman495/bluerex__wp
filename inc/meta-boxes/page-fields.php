<?php

function page_fields() {
	add_meta_box( 'extra_fields', 'Дополнительные поля', 'page_fields_box_func', 'page', 'normal', 'high'  );
}
add_action('add_meta_boxes', 'page_fields', 1);

function page_fields_box_func( $post )
{
    wp_nonce_field(plugin_basename(__FILE__), 'wp_custom_attachment_nonce');
	?>
	<p>
        <label><b><?php _e( 'Заголовок страницы (title)', 'bluerex' ) ?></b></label>
        <input
            type="text" 
            name="extra[title]" 
            value="<?php echo get_post_meta( $post->ID, 'title', 1 ); ?>"
            style="width:100%"
        >
    </p>
	<p>
        <label><b><?php _e( 'Подзаголовок страницы (subtitle)', 'bluerex' ) ?></b></label>
        <input
            type="text" 
            name="extra[subtitle]" 
            value="<?php echo get_post_meta( $post->ID, 'subtitle', 1 ); ?>"
            style="width:100%"
        >
    </p>
	<p>
        <label><b><?php _e( 'Описание статьи (description)', 'bluerex' ) ?></b></label>
        <textarea 
            type="text" 
            name="extra[description]"
            style="width:100%;height:50px;"
        ><?php echo get_post_meta($post->ID, 'description', 1); ?></textarea>
	</p>
    <p>
        <label><b><?php _e( 'Адрес картинки в медиатеке (img)', 'bluerex' ) ?></b></label>
        <input
            type="text" 
            name="extra[img]"
            value="<?php echo get_post_meta( $post->ID, 'img', 1 ); ?>"
            style="width:100%"
        >
    </p>
	<p>
        <label><b><?php _e( 'Имя ссылки 1 (link_title_1)', 'bluerex' ) ?></b></label>
        <input
            type="text" 
            name="extra[link_title_1]" 
            value="<?php echo get_post_meta( $post->ID, 'link_title_1', 1 ); ?>"
            style="width:100%"
        >
    </p>
	<p>
        <label><b><?php _e( 'Адрес ссылки 1 (link_href_1)', 'bluerex' ) ?></b></label>
        <input
            type="text" 
            name="extra[link_href_1]" 
            value="<?php echo get_post_meta( $post->ID, 'link_href_1', 1 ); ?>"
            style="width:100%"
        >
    </p>
	<p>
        <label><b><?php _e( 'Имя ссылки 2 (link_title_2)', 'bluerex' ) ?></b></label>
        <input
            type="text" 
            name="extra[link_title_2]" 
            value="<?php echo get_post_meta( $post->ID, 'link_title_2', 1 ); ?>"
            style="width:100%"
        >
    </p>
	<p>
        <label><b><?php _e( 'Адрес ссылки 2 (link_href_2)', 'bluerex' ) ?></b></label>
        <input
            type="text" 
            name="extra[link_href_2]" 
            value="<?php echo get_post_meta( $post->ID, 'link_href_2', 1 ); ?>"
            style="width:100%"
        >
    </p>
    <input
        type="hidden" 
        name="extra_fields_nonce" 
        value="<?php echo wp_create_nonce(__FILE__); ?>"
    >
	<?php
}

function page_fields_update( $post_id )
{
	// базовая проверка
	if (
		   empty( $_POST['extra'] )
		|| ! wp_verify_nonce( $_POST['extra_fields_nonce'], __FILE__ )
		|| wp_is_post_autosave( $post_id )
		|| wp_is_post_revision( $post_id )
	)
		return false;

	// Все ОК! Теперь, нужно сохранить/удалить данные
	$_POST['extra'] = array_map( 'sanitize_text_field', $_POST['extra'] ); // чистим все данные от пробелов по краям
	foreach( $_POST['extra'] as $key => $value ){
		if( empty($value) ){
			delete_post_meta( $post_id, $key ); // удаляем поле если значение пустое
			continue;
		}

		update_post_meta( $post_id, $key, $value ); // add_post_meta() работает автоматически
	}
 
	return $post_id;
}
add_action( 'save_post', 'page_fields_update', 0 );
