<?php

new Orders_Metabox;

class Orders_Metabox
{
    public static $post_type = 'orders';

    public function __construct()
    {
        add_action( 'add_meta_boxes', [ $this, 'add_metabox' ] );
        add_action( 'save_post', [ $this, 'save_metabox' ] );
    }

    public function add_metabox() 
    {
        add_meta_box( 
            'orders_fields', 
            __( 'Данные клиента', 'bluerex' ),
            [ $this, 'render_metabox' ], 
            self::$post_type, 
            'normal', 
            'low'  
        );
    }

    public function render_metabox( $post ) 
    {
        $fields = [
            [
                'id'          => 'date',
                'value'       => date( "F j, Y, g:i a", strtotime( $post->post_date ) ),
                'type'        => 'text',
                'disabled'    => true,
                'label'       => __( 'Дата заявки', 'bluerex' ),
            ],
            [
                'id'          => 'name',
                'type'        => 'text',
                'disabled'    => true,
                'value'       => get_post_meta( $post->ID, 'name', 1 ),
                'label'       => __( 'Имя клиента', 'bluerex' ),
            ],
            [
                'id'          => 'email',
                'type'        => 'text',
                'disabled'    => true,
                'value'       => get_post_meta( $post->ID, 'email', 1 ),
                'label'       => __( 'Email клиента', 'bluerex' ),
            ],
            [
                'id'          => 'comment',
                'type'        => 'textarea',
                'label'       => __( 'Комментарий', 'bluerex' ),
                'placeholder' => __( 'Комментарий', 'bluerex' ),
            ],
            [
                'id'          => 'status',
                'type'        => 'select',
                'options'     => [
                    'new'        => __( 'Новая заявка', 'bluerex' ),
                    'done'       => __( 'Заявка обработана', 'bluerex' ),
                    'processing' => __( 'Требуется уточнение', 'bluerex' ),
                ],
                'label'       => __( 'Статус заявки', 'bluerex' ),
                'placeholder' => __( 'Статус заявки', 'bluerex' ),
            ],
        ];
		?>
		<table class="form-table">
            <tbody>
                <?php foreach( $fields as $field ) : ?>
                    <tr>
                        <th scope="row">
                            <label for="<?php echo self::$post_type . '_' . $field['id'] ?>"><?php echo $field['label'] ?></label>
                        </th>
                        <td>
                            <?php if( $field['type'] == 'text' ) : ?>
                                <input 
                                    name="<?php echo self::$post_type . '[' . $field['id'] . ']' ?>" 
                                    type="text" 
                                    id="<?php echo self::$post_type . '_' . $field['id'] ?>" 
                                    value="<?php echo $field['value'] ?? '' ?>" 
                                    placeholder="<?php echo $field['placeholder'] ?? '' ?>" 
                                    class="regular-text"
                                    style="width:100%"
                                    <?php if( $field['disabled'] ) : ?>
                                        disabled
                                    <?php endif ?>
                                >
                            <?php elseif( $field['type'] == 'textarea' ) : ?>
                                <textarea 
                                    name="<?php echo self::$post_type . '[' . $field['id'] . ']' ?>" 
                                    id="<?php echo self::$post_type . '_' . $field['id'] ?>"
                                    class="metavalue" 
                                    rows="2"
                                    style="width:100%"
                                ><?php echo get_post_meta( $post->ID, $field['id'], 1 ); ?></textarea>
                            <?php elseif( $field['type'] == 'select' ) : ?>
                                <label for="<?php echo self::$post_type . '_' . $field['id'] ?>">
                                    <select 
                                        name="<?php echo self::$post_type . '[' . $field['id'] . ']' ?>" 
                                        id="<?php echo self::$post_type . '_' . $field['id'] ?>"
                                    >
                                        <option>...</option>
                                        <?php foreach( $field['options'] as $key => $value ) : ?>
                                            <option 
                                                value="<?php echo $key ?>"
                                                <?php selected( $key, get_post_meta( $post->ID, $field['id'], 1 ) ) ?>
                                            >
                                                <?php echo $value ?>
                                            </option>
                                        <?php endforeach ?>
                                    </select>
                                </label>
                            <?php endif ?>
                            <br>
                            <?php if( isset( $field['description'] ) ) : ?>
                                <span class="description"><?php echo $field['description'] ?></span>
                            <?php endif ?>
                        </td>
                    </tr>
                <?php endforeach ?>
            </tbody>
		</table>
		<?php
	}

    public function save_metabox( $post_id ) 
    {
		if ( wp_is_post_autosave( $post_id ) )
			return;

        if ( isset( $_POST[self::$post_type] ) && is_array( $_POST[self::$post_type] ) ) {
            $_POST[self::$post_type] = array_map( 'sanitize_text_field', $_POST[self::$post_type] );
        
            foreach( $_POST[self::$post_type] as $key => $value ){
                if( empty($value) ){
                    delete_post_meta( $post_id, $key );
                    continue;
                }
                update_post_meta( $post_id, $key, $value );
            }    
        }
	}
}