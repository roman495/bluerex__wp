<?php get_header() ?>
<?php
if ( shortcode_exists( 'bx_breadcrumbs' ) ) {
    echo do_shortcode( '[bx_breadcrumbs]' );
} 
?>
<section class="section section-content">
    <div class="container">
        <div class="row">
            <main class="col-md-8">
                <?php if( have_posts() ) : ?>
					<h1>
						<?php the_title() ?>
					</h1>
					<?php the_content() ?>
                <?php else : ?>
                    <p class="lead"><?php _e( 'Нет постов', 'bluerex' ) ?></p>
                <?php endif ?>
            </main>
            <aside class="col-md-4">
                <?php get_sidebar() ?>
            </aside>    
        </div>
    </div>
</section>
<!-- /.section section-content -->

<?php get_footer() ?>