<?php
/**
 * bluerex functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package bluerex
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'bluerex_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function bluerex_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on bluerex, use a find and replace
		 * to change 'bluerex' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'bluerex', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Header menu', 'bluerex' ),
				'menu-2' => esc_html__( 'Bottom menu', 'bluerex' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'bluerex_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);

		// Add Woocommerce
		add_theme_support( 'woocommerce' );
	}
endif;
add_action( 'after_setup_theme', 'bluerex_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function bluerex_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'bluerex_content_width', 640 );
}
add_action( 'after_setup_theme', 'bluerex_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function bluerex_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'bluerex' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'bluerex' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);

	register_sidebar(
		array(
			'name'          => esc_html__( 'Footer left', 'bluerex' ),
			'id'            => 'footer',
			'description'   => esc_html__( 'Левый футер сайта', 'bluerex' ),
			'before_widget' => '<div id="%1$s" class="widget col-6 %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h5>',
			'after_title'   => '</h5>',
		)
	);

	register_sidebar(
		array(
			'name'          => esc_html__( 'Footer right', 'bluerex' ),
			'id'            => 'footer-2',
			'description'   => esc_html__( 'Правый футер сайта', 'bluerex' ),
			'before_widget' => '<div id="%1$s" class="widget col-md-6 footer-images %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h5>',
			'after_title'   => '</h5>',
		)
	);

	register_sidebar(
		array(
			'name'			=> esc_html__( 'Product filter', 'bluerex' ),
			'id'			=> 'product-filter',
		)
	);

	require get_template_directory() . '/inc/widgets/class-bx-widget-media-gallery.php';
	require get_template_directory() . '/inc/widgets/class-bx-widget-category.php';
	require get_template_directory() . '/inc/widgets/class-bx-widget-product-category.php';
	require get_template_directory() . '/inc/widgets/class-bx-widget-product-find.php';

	register_widget( 'BluerexGallery_Widget' );
	register_widget( 'Bluerex_Category_Widget' );
	register_widget( 'Bluerex_Product_Category_Widget' );
	register_widget( 'Bluerex_Product_Find_Widget' );
}
add_action( 'widgets_init', 'bluerex_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function bluerex_scripts() {
	wp_enqueue_style( 'bluerex-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_style_add_data( 'bluerex-style', 'rtl', 'replace' );

	wp_enqueue_style( 'bluerex-montserrat', 'https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;600;700&display=swap' );
	wp_enqueue_style( 'bluerex-poppins', 'https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;600;700&display=swap' );
	wp_enqueue_style( 'bluerex-bootstrap', 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css' );
	wp_enqueue_style( 'bluerex-fa', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css' );
	wp_enqueue_style( 'bluerex-baguette', 'https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.11.1/baguetteBox.min.css' );

	wp_enqueue_style( 'bluerex-main', get_template_directory_uri() . '/assets/css/style.css' );

	wp_enqueue_script( 'bluerex-navigation', get_template_directory_uri() . '/assets/js/navigation.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'bluerex-bootstrap', 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'bluerex-baguette', 'https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.11.1/baguetteBox.min.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'bluerex-sweetalert', get_template_directory_uri() . '/node_modules/sweetalert/dist/sweetalert.min.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'bluerex-main', get_template_directory_uri() . '/assets/js/main.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'bluerex-shop', get_template_directory_uri() . '/assets/js/shop.js', array(), _S_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	wp_localize_script( 
		'bluerex-main', 
		'bxVars', 
		[ 
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
			'nonce'   => wp_create_nonce( 'bluerex_nonce' ), 
		] 
	);
}
add_action( 'wp_enqueue_scripts', 'bluerex_scripts' );

add_image_size( 'bluerex_portfolio-small', 300, 300, true );

function new_excerpt_more( $more )
{
	return '';
}
add_filter( 'excerpt_more', 'new_excerpt_more' );

function span_shortcode( $atts, $content ) {
	return '<span>' . $content . '</span>';
}
add_shortcode( 'span', 'span_shortcode' );

function br_shortcode( $atts, $content ) {
	return '<br>';
}
add_shortcode( 'br', 'br_shortcode' );

function bx_navigation_template( $template, $class ){
	return '<nav class="mt-5" aria-label="Pagination">%3$s</nav>';
}
add_filter( 'navigation_markup_template', 'bx_navigation_template', 10, 2 );

/**
 * Helpers functions
 */
require get_template_directory() . '/inc/helpers.php';

/**
 * Custom fields
 */
require get_template_directory() . '/inc/meta-boxes/page-fields.php';
require get_template_directory() . '/inc/meta-boxes/progress-fields.php';
require get_template_directory() . '/inc/meta-boxes/services-fields.php';
require get_template_directory() . '/inc/meta-boxes/testimonials-fields.php';
require get_template_directory() . '/inc/meta-boxes/orders-fields.php';

require get_template_directory() . '/inc/Bluerex_Header_Menu.php';

/**
 * Custom edit list form
 */
require get_template_directory() . '/inc/edit_list_form/orders_list_form.php';

/**
 * Custom post type
 */
require get_template_directory() . '/inc/custom_post_types.php';

require get_template_directory() . '/inc/custom-post-columns.php';

add_action( 'init', 'custom_post_types', 0 );

/**
 * Custom taxonomy
 */
require get_template_directory() . '/inc/custom-taxonomy.php';

/**
 * Form
 */
require get_template_directory() . '/inc/form.php';

/**
 * Admin Options
 */
require get_template_directory() . '/inc/admin-options.php';

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/inc/woocommerce.php';
}

require get_template_directory() . '/inc/widgets/product-favorites/widget.php';

require get_template_directory() . '/inc/auth.php';
