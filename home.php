<?php get_header() ?>
<?php
if ( shortcode_exists( 'bx_breadcrumbs' ) ) {
    echo do_shortcode( '[bx_breadcrumbs]' );
} 
?>
<section class="section section-content">
    <div class="container">
        <div class="row">
            <main class="col-md-8">
                <?php if( have_posts() ) : ?>
                    <div class="row row-cols-1 row-cols-sm-2 row-cols-md-2 row-cols-lg-3 g-4">
                    <?php while( have_posts() ) : the_post() ?>       
                        <div class="col">
                            <a href="<?php the_permalink() ?>">
                                <div class="card h-100">
                                    <img 
                                        src="<?php echo has_post_thumbnail() ? get_the_post_thumbnail_url() : 'https://fakeimg.pl/1600x900/?text=BlueRex&font=lobster' ?>" 
                                        class="card-img-top" 
                                        alt="<?php echo get_the_title() ?>"
                                    >
                                    <div class="card-body">
                                        <h5 class="card-title"><?php the_title() ?></h5>
                                        <p class="card-text"><?php the_excerpt() ?></p>
                                    </div>
                                    <div class="card-footer">
                                        <small class="text-muted">Last updated <?php the_time( 'd.m.Y' ) ?></small>
                                    </div>
                                </div>
                            </a>
                        </div>
                    <?php endwhile ?>    
                    </div>
                    <?php the_posts_pagination( [
                        'type'      => 'list',
                        'prev_text' => '<span aria-hidden="true">&laquo;</span>',
                        'next_text' => '<span aria-hidden="true">&raquo;</span>',
                    ] ) ?>
                <?php else : ?>
                    <h1><?php _e( 'Нет постов', 'bluerex' ) ?></h1>
                <?php endif ?>
            </main>
            <aside class="col-md-4">
                <?php get_sidebar() ?>
            </aside>    
        </div>
    </div>
</section>
<!-- /.section section-content -->

<?php get_footer() ?>