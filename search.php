<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package bluerex
 */

get_header();	
?>
<?php
if ( shortcode_exists( 'bx_breadcrumbs' ) ) {
    echo do_shortcode( '[bx_breadcrumbs]' );
} 
?>
<div class="container-fluid mt-3">
    <h1 class="text-center"><?php woocommerce_page_title() ?></h1>
    <div class="row">
        <aside class="col-sm-12 col-md-4 col-lg-3">
            <div class="d-flex justify-content-between d-sm-none my-3">
                <a class="btn btn-link text-decoration-none" data-bs-toggle="collapse" href="#productCatelog" role="button" aria-expanded="false" aria-controls="collapseExample">
                    Product catalog <i class="fas fa-caret-down"></i>
                </a>
                <a class="btn btn-link text-decoration-none" data-bs-toggle="collapse" href="#findProduct" role="button" aria-expanded="false" aria-controls="collapseExample">
                    Find Products <i class="fas fa-caret-down"></i>
                </a>
            </div>
            <?php
                if ( is_active_sidebar( 'product-filter' ) ) {
                    dynamic_sidebar( 'product-filter' );
                }                
            ?>
        </aside>
        <main class="col-sm-12 col-md-8 col-lg-9">
            <?php
                $query = new WP_Query( [
                    'post_type'      => 'product',
                    'posts_per_page' => 12,
					's'				 => get_query_var( 's' ),
                    // 'meta_key'      => '_featured',
                    // 'meta_value'    => 'yes',
                ] );

                if( $query->have_posts() ) {
                    woocommerce_product_loop_start();
                    
                    while( $query->have_posts() ) {
                        
                        $query->the_post();
                        
                        wc_get_template_part( 'content', 'product' );
                    }
                    wp_reset_postdata();
                    woocommerce_product_loop_end();
                }
            ?>
        </main>
    </div>
</div>
<?php get_footer() ?>
