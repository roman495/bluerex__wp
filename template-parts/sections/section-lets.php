<?php
$page_id = '91'; // Page Let's
$post = get_posts( [ 'post_type' => 'page', 'include' => $page_id ] )[0];
?>
<section 
    class="section section-lets text-center"
    style="background: url(<?= has_post_thumbnail() ? get_the_post_thumbnail_url() : '' ?>) center no-repeat; background-size: cover;"
>
    <div class="container">
        <div class="row">
            <div class="col-md-10 offset-md-1">
                <h3><?= get_post_meta( $page_id, 'title', true ) ?></h3>
                <h4><?= get_post_meta( $page_id, 'subtitle', true ) ?></h4>
                <p><?= get_post_meta( $page_id, 'description', true ) ?></p>
                <p><a href="<?= get_permalink( $page_id ) ?>" class="btn btn-pink btn-shadow">read more</a></p>
            </div>
        </div>
    </div>
</section>
<?php wp_reset_postdata(); ?>