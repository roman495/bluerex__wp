<?php
$page_id = '131'; // Page Our Happy Clients
$post = get_posts( [ 
    'post_type' => 'page', 
    'include'   => $page_id 
] )[0];

$testimonials = get_posts( [ 
    'post_type' => 'testimonials', 
    'orderby'   => 'date',
    'order'     => 'ASC',      
] );
?>
<section
    class="section section-reviews"
    style="background: url(<?= has_post_thumbnail() ? get_the_post_thumbnail_url() : '' ?>) center no-repeat;background-size: cover;"
>
    <div id="carouselReviews" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-indicators">
            <?php for( $i = 0; $i < count( $testimonials ); $i++ ) : ?>
                <button 
                    type="button" 
                    data-bs-target="#carouselReviews" 
                    data-bs-slide-to="<?= $i ?>"
                    <?php if ( $i == 0 ) : ?>
                        class="active" 
                    <?php endif ?>
                    aria-current="<?= ( $i == 0 ) ? 'true' : 'false' ?>" 
                    aria-label="Slide <?= $i + 1 ?>"
                ></button>
            <?php endfor ?>
        </div>
        <div class="carousel-inner">
            <?php foreach( $testimonials as $key => $testimonial ) : ?>
                <div class="carousel-item <?= ( $key == 0 ) ? 'active' : '' ?>">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-7">
                                <div class="carousel-caption">
                                    <h3><?= get_post_meta( $page_id, 'title', true ) ?></h3>
                                    <h4><?= get_post_meta( $page_id, 'subtitle', true ) ?></h4>
                                    <figure>
                                        <blockquote class="blockquote">
                                            <p class="mb-0"><?= $testimonial->post_excerpt ?></p>
                                        </blockquote>
                                        <figcaption class="blockquote-footer">
                                            <cite title="Source Title"><?= get_post_meta( $testimonial->ID, 'client', true ) ?></cite>
                                        </figcaption>
                                    </figure>
                                </div>
                            </div>
                            <div class="col-sm-5 d-none d-sm-block">
                                <img 
                                    src="<?= get_post_meta( $page_id, 'img', true ) ?>" 
                                    alt="<?= get_post_meta( $page_id, 'title', true ) ?>"
                                >
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselReviews" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselReviews" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>
</section>
<!-- /.section section-reviews -->
