<?php
$page_id = '94'; // Page Design
$post = get_posts( [ 'post_type' => 'page', 'include' => $page_id ] )[0];

$services = get_posts( [ 'post_type' => 'services', 'numberposts' => '2' ] );
?>
    <section class="section section-design">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <h3><?= get_post_meta( $page_id, 'title', true ) ?></h3>
                    <h4><?= get_post_meta( $page_id, 'subtitle', true ) ?></h4>
                    <p><?= get_post_meta( $page_id, 'description', true ) ?></p>
                    <div class="row">
                        <?php foreach( $services as $service ) : ?>
                            <div class="col-md-6 mb-3">
                                <i class="<?= get_post_meta( $service->ID, 'icon', true ) ?>"></i>
                                <h5><?= $service->post_title  ?></h5>
                                <p><?= $service->post_excerpt ?></p>
                                <p><a href="<?= get_permalink( $service->ID ) ?>" class="btn btn-pink btn-shadow">read more</a></p>
                            </div>
                        <?php endforeach ?>
                        <?php wp_reset_postdata(); ?>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="ratio ratio-16x9">
                        <iframe
                            src="https://www.youtube.com/embed/<?= get_post_meta( $page_id, 'img', true ) ?>"
                            title="YouTube video player"
                            frameborder="0"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen
                        ></iframe>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.section section-design -->
<?php wp_reset_postdata(); ?>
