<?php
$posts = get_posts( [ 
    'post_type' => 'progress', 
    'numberposts' => '3',
    'orderby' => 'date',
    'order' => 'ASC',      
] );
?>    
<section class="section section-progress text-center">
    <div class="container-fluid">
        <div class="row">
            <?php foreach ( $posts as $post ) : ?>
                <div class="col-md-4 progress-item">
                    <div class="icon"><i class="<?= get_post_meta( $post->ID, 'icon', true ) ?>"></i></div>
                    <div class="count"><?= get_post_meta( $post->ID, 'counter', true ) ?></div>
                    <h4 class="title"><?= do_shortcode( $post->post_title ) ?></p></h4>
                    <p class="text"><?= $post->post_content ?></p>
                </div>
            <?php endforeach ?>
        </div>
    </div>
</section>
<?php wp_reset_postdata() ?>