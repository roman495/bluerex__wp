<?php
$page_id = '97'; // Page Our Recent Work
$page = get_posts( [ 'post_type' => 'page', 'include' => $page_id ] )[0];

$terms = get_terms( [
	'taxonomy' => 'worktype',
	'hide_empty' => false,
] );
?>
    <section class="section section-work section-tabs">
        <div class="container">
            <div class="row">
                <div class="col-md-8 offset-md-2 text-center">
                    <h4><?php echo $page->post_title ?></h4>
                    <?php echo $page->post_content ?>
                </div>
                <div class="col-md-12">
                    <ul class="nav nav-pills justify-content-center mt-3 mb-5" id="pills-tab-gallary" role="tablist">
                        <?php foreach ( $terms as $key => $term ) : ?>
                            <li class="nav-item" role="presentation">
                                <button 
                                    class="nav-link rounded-pill <?= $key == 0 ? 'active' : '' ?>" 
                                    type="button" 
                                    role="tab" 
                                    id="pills-<?= $term->slug ?>-tab-gallary" 
                                    data-bs-toggle="pill" 
                                    data-bs-target="#pills-<?= $term->slug ?>-gallary" 
                                    aria-controls="pills-<?= $term->slug ?>-gallary" 
                                    aria-selected="<?= $key == 0 ? 'true' : 'false' ?>"
                                >
                                    <?= $term->name ?>
                                </button>
                            </li>
                        <?php endforeach ?>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <?php foreach ( $terms as $key => $term ) : ?>
                            <?php
                            $portfolio = new WP_Query( [
                                'post_type' => 'portfolio',
                                'tax_query' => [
                                    [
                                        'taxonomy' => 'worktype',
                                        'field'    => 'slug',
                                        'terms'    => $term->slug,
                                    ]
                                ],
                            ] );
                            ?>
                            <div
                                class="tab-pane fade show <?= $key == 0 ? 'active' : '' ?>" 
                                id="pills-<?= $term->slug ?>-gallary" 
                                role="tabpanel" 
                                aria-labelledby="pills-<?= $term->slug ?>-tab-gallary"
                            >
                                <div class="gallary text-center row row-cols-md-3 row-cols-sm-1">
                                    <?php while( $portfolio->have_posts() ) : $portfolio->the_post(); ?>
                                        <div class="col gallary-item">
                                            <a href="<?php echo get_the_post_thumbnail_url( $portfolio->ID, 'full' ) ?>">
                                                <img src="<?php echo get_the_post_thumbnail_url( $portfolio->ID, 'bluerex_portfolio-small' ) ?>" alt="">
                                            </a>
                                        </div>
                                    <?php endwhile ?>
                                    <?php wp_reset_postdata(); ?>
                                </div>
                            </div>
                        <?php endforeach ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.section section-work -->
