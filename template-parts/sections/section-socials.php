
<section class="section section-socials">
    <div class="container text-center">
        <a href="//<?php echo get_option( 'bx_opiton_field_twitter' ) ?>">twitter</a>
        <a href="//<?php echo get_option( 'bx_opiton_field_facebook' ) ?>">facebook</a>
        <a href="//<?php echo get_option( 'bx_opiton_field_instagram' ) ?>">instagram</a>
        <a href="//<?php echo get_option( 'bx_opiton_field_youtube' ) ?>">youtube</a>
        <a href="//<?php echo get_option( 'bx_opiton_field_linkedin' ) ?>">linkedin</a>
    </div>
</section>
<!-- /.section section-socials -->
