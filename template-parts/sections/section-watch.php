<?php
$page_id = '80'; // Page watch
$post = get_posts( [ 'post_type' => 'page', 'include' => $page_id ] )[0];

$terms = get_terms( [
	'taxonomy' => 'worktype',
	'hide_empty' => false,
] );
?>
<section 
    class="section section-watch section-tabs"
    style="background: url(<?= has_post_thumbnail() ? get_the_post_thumbnail_url() : '' ?>) center no-repeat;"
>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12">
                <h3><?= get_post_meta( $page_id, 'title', true ) ?></h3>
                <h4><?= get_post_meta( $page_id, 'subtitle', true ) ?></h4>
                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                    <?php foreach ( $terms as $key => $term ) : ?>
                        <li class="nav-item" role="presentation">
                            <button 
                                class="nav-link rounded-pill <?= $key == 0 ? 'active' : '' ?>" 
                                type="button" 
                                role="tab" 
                                id="pills-<?= $term->slug ?>-tab" 
                                data-bs-toggle="pill" 
                                data-bs-target="#pills-<?= $term->slug ?>" 
                                aria-controls="pills-<?= $term->slug ?>" 
                                aria-selected="<?= $key == 0 ? 'true' : 'false' ?>"
                            >
                                <?= $term->name ?>
                            </button>
                        </li>
                    <?php endforeach ?>
                </ul>                

                <div class="tab-content" id="pills-tabContent">
                    <?php foreach ( $terms as $key => $term ) : ?>
                        <?php
                            $works = new WP_Query( array(
                                'post_type' => 'services',
                                'tax_query' => array(
                                    array(
                                        'taxonomy' => 'worktype',
                                        'field'    => 'slug',
                                        'terms'    => $term->slug,
                                    ),
                                ),
                            ) );
                            if ( $works->have_posts() ) : $works->the_post();
                        ?>
                                <div
                                    class="tab-pane fade show <?= $key == 0 ? 'active' : '' ?>" 
                                    id="pills-<?= $term->slug ?>" 
                                    role="tabpanel" 
                                    aria-labelledby="pills-<?= $term->slug ?>-tab"
                                >
                                    <?php the_excerpt(); ?>
                                    <p><a class="btn btn-pink btn-shadow" href="<?=get_permalink( get_the_ID() )?>">read more</a></p>
                                </div>
                        <?php endif ?>
                        <?php wp_reset_postdata(); ?>
                    <?php endforeach ?>

                </div>
            </div>
            <div class="col-lg-6 d-none d-lg-block text-center">
                <img 
                    src="<?= get_post_meta( $page_id, 'img', true ) ?>" 
                    alt="<?= get_post_meta( $page_id, 'title', true ) ?>"
                >
            </div>
        </div>
    </div>
</section>
