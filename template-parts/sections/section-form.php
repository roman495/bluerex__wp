<?php
$page_id = '140'; // Page Contact Form
$post = get_page_by_path( 'contact-form', 'OBJECT', 'page' );
?>
<section 
    class="section section-form text-center"
    <?php if( has_post_thumbnail() ) : ?>
        style="background:url(<?= get_the_post_thumbnail_url() ?>) center no-repeat;background-size:cover;"
    <?php endif ?>
>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4><?= get_post_meta( $page_id, 'title', true ) ?></h4>
                <h5><?= get_post_meta( $page_id, 'subtitle', true ) ?></h5>
                <p><?= get_post_meta( $page_id, 'description', true ) ?></p>
                <form
                    id="contactForm"
                    method="POST"
                    action="<?php echo esc_url( admin_url( 'admin-post.php' ) ) ?>" 
                    class="text-left"
                >
                    <input type="hidden" name="action" value="bluerex_form">
                    <div class="row">
                        <div class="col-md-5">
                            <input name="bluerex_name" type="text" class="form-control" placeholder="Name" aria-label="Name">
                        </div>
                        <div class="col-md-5">
                            <input name="bluerex_email" type="email" class="form-control" placeholder="Email" aria-label="Email">
                        </div>
                        <div class="col-md-2">
                            <button
                                id="btnContactForm" 
                                type="submit" 
                                class="btn btn-violet btn-shadow-violet"
                                data-url="<?php echo esc_url( admin_url( 'admin-ajax.php' ) ) ?>"
                            >
                                Submit
                            </button>
                            <div id="spinnerContactForm" class="spinner-border d-none text-violet" style="width: 3rem; height: 3rem;" role="status">
                                <span class="visually-hidden">Loading...</span>
                            </div>
                        </div>
                        <script>
                            window.addEventListener('load', function() {
                                const contactForm = document.getElementById('contactForm')
                                const btnContactForm = document.getElementById('btnContactForm')
                                const spinnerContactForm = document.getElementById('spinnerContactForm')

                                btnContactForm.addEventListener('click', e => {
                                    e.preventDefault()
                                    
                                    btnContactForm.classList.add('d-none')
                                    spinnerContactForm.classList.remove('d-none')

                                    const data = new FormData(contactForm)
                                    data.append('action', 'bluerex_form')

                                    fetch(e.target.dataset.url, {
                                        method: 'POST',
                                        body: data
                                    })
                                    .then(response => response.text())
                                    .then(result => {
                                        swal({
                                            text: result,
                                            icon: 'success'
                                        })
                                        
                                        btnContactForm.classList.remove('d-none')
                                        spinnerContactForm.classList.add('d-none')

                                        document.getElementById('contactForm').reset()
                                    })
                                })

                            });
                        </script>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- /.section section-form -->
