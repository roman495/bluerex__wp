<?php if ( is_shop() && is_active_sidebar( 'product-bottom' ) ) : ?>
	<div class="container-fluid my-5">
		<?php dynamic_sidebar( 'product-bottom' ); ?>
	</div>
<?php endif ?>

<footer class="page-footer">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<?php if ( is_active_sidebar( 'footer' ) ) : ?>
					<div class="row">
						<?php dynamic_sidebar( 'footer' ); ?>
					</div>
				<?php endif ?>
			</div>
			<div class="col-md-6 footer-images">
				<?php 
					if ( is_active_sidebar( 'footer-2' ) ) {
						dynamic_sidebar( 'footer-2' );
					} 
				?>
			</div>
		</div>
	</div>

	<div class="footer-copyright mt-5">
		<ul class="d-flex justify-content-center flex-wrap">
			<li><span id="bxFooterCopyright"><?php echo get_theme_mod( 'bxFooterCopyright' ) ?></span></li>
			<li><a id="bxFooterPrivacy" href="//<?php echo get_theme_mod( 'bxFooterPrivacy' ) ?>">Privacy</a></li>
			<li><a id="bxFooterTerm" href="//<?php echo get_theme_mod( 'bxFooterTerm' ) ?>">Term Of Us</a></li>
			<li><a id="bxFooterSitemap" href="//<?php echo get_theme_mod( 'bxFooterSitemap' ) ?>">Site Map</a></li>
		</ul>
	</div>

</footer>

<div class="modal fade" id="signUpModal" tabindex="-1" aria-labelledby="signUpModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="signUpModalLabel">Sign Up</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
		<form id="signUpForm">
			<div class="mb-3">
				<label for="bxSignUpLogin" class="form-label">Login</label>
				<input name="bxSignUpLogin" type="text" class="form-control" id="bxSignUpLogin" data-valid="notEmpty">
			</div>
			<div class="mb-3">
				<label for="bxSignUpEmail" class="form-label">Email address</label>
				<input name="bxSignUpEmail" type="email" class="form-control" id="bxSignUpEmail" data-valid="email">
			</div>
			<div class="mb-3">
				<label for="bxSignUpPassword" class="form-label">Password</label>
				<input name="bxSignUpPassword" type="password" class="form-control" id="bxSignUpPassword" data-valid="notEmpty">
			</div>
			<div class="mb-3">
				<label for="bxSignUpConfirmPassword" class="form-label">Confirm Password</label>
				<input name="bxSignUpConfirmPassword" type="password" class="form-control" id="bxSignUpConfirmPassword" data-valid="notEmpty">
			</div>
		</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button id="signUpSubmit" type="button" class="btn btn-primary">Sign Up</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="signInModal" tabindex="-1" aria-labelledby="signInModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="signInModalLabel">Sign Up</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
		<form id="signInForm">
			<div class="mb-3">
				<label for="bxSignInLogin" class="form-label">Login</label>
				<input name="bxSignInLogin" type="text" class="form-control" id="bxSignInLogin" data-valid="notEmpty">
			</div>
			<div class="mb-3">
				<label for="bxSignInPassword" class="form-label">Password</label>
				<input name="bxSignInPassword" type="password" class="form-control" id="bxSignInPassword" data-valid="notEmpty">
			</div>
		</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button id="signInSubmit" type="button" class="btn btn-primary">Sign In</button>
      </div>
    </div>
  </div>
</div>

<?php wp_footer(); ?>
</body>
</html>
