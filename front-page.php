<?php get_header() ?>

<?php get_template_part( 'template-parts/sections/section', 'watch' ) ?>

<?php get_template_part( 'template-parts/sections/section', 'progress' ) ?>

<?php get_template_part( 'template-parts/sections/section', 'lets' ) ?>

<?php get_template_part( 'template-parts/sections/section', 'design' ) ?>

    <section class="section section-brands">
        <div class="container">
            <div class="icons">
                <div>
                    <i class="icon fab fa-amazon"></i>
                </div>
                <div>
                    <i class="icon fab fa-angular"></i>
                </div>
                <div>
                    <i class="icon fab fa-edge"></i>
                </div>
                <div>
                    <i class="icon fab fa-facebook"></i>
                </div>
                <div>
                    <i class="icon fab fa-git-alt"></i>
                </div>
                <div>
                    <i class="icon fab fa-google-drive"></i>
                </div>
                <div>
                    <i class="icon fab fa-medium-m"></i>
                </div>
                <div>
                    <i class="icon fab fa-java"></i>
                </div>
            </div>
        </div>
    </section>
    <!-- /.section section-brands -->

<?php get_template_part( 'template-parts/sections/section', 'work' ) ?>

<?php get_template_part( 'template-parts/sections/section', 'reviews' ) ?>

<?php get_template_part( 'template-parts/sections/section', 'form' ) ?>

<?php get_template_part( 'template-parts/sections/section', 'socials' ) ?>

<?php get_footer() ?>