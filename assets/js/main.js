window.addEventListener('load', () => {
    baguetteBox.run('.gallary');
    baguetteBox.run('.gallery_widget');

    const scrollBtn = document.getElementById('scrollToTop')

    let scrollTimeout;

    window.addEventListener('scroll', e => {
        clearTimeout(scrollTimeout)
        scrollTimeout = setTimeout(onScroll, 200)
    })

    function onScroll() {
        if (window.pageYOffset > window.innerHeight) {
            scrollBtn.classList.add('d-block')
        } else {
            scrollBtn.classList.remove('d-block')
        }
    }

    scrollBtn.addEventListener('click', e => {
        window.scrollTo({ top: 0, behavior: 'smooth' })
    })
    
    // Sign Up 
    const signUpBtn    = document.querySelector('#signUpBtn')
    const signInBtn    = document.querySelector('#signInBtn')
    const signUpForm   = document.querySelector('#signUpForm')
    const signInForm   = document.querySelector('#signInForm')
    const inputs       = document.querySelectorAll('#signUpForm .form-control')
    const inputs2      = document.querySelectorAll('#signInForm .form-control')
    const signUpSubmit = document.querySelector('#signUpSubmit')
    const signInSubmit = document.querySelector('#signInSubmit')
    const signUpModal  = new bootstrap.Modal(document.querySelector('#signUpModal'), {
        keyboard: true
    })
    const signInModal  = new bootstrap.Modal(document.querySelector('#signInModal'), {
        keyboard: true
    })

    const patterns = {
        notEmpty: /.+/,
        email: /^.+@.+\..+$/
    }

    signUpForm.addEventListener('focusin', e => {
        if (e.target.classList.contains('form-control')) {
            e.target.classList.remove('is-invalid')
            e.target.classList.remove('is-valid')
        }
    })

    signInForm.addEventListener('focusin', e => {
        if (e.target.classList.contains('form-control')) {
            e.target.classList.remove('is-invalid')
            e.target.classList.remove('is-valid')
        }
    })

    signUpBtn.addEventListener('click', e => {
        e.preventDefault()
        signUpModal.show()
    })

    signInBtn.addEventListener('click', e => {
        e.preventDefault()
        signInModal.show()
    })

    signUpSubmit.addEventListener('click', async e => {
        e.preventDefault()
        e.target.disabled = true

        // Validation 
        isValid = true
        inputs.forEach(i => {
            let pattern = patterns[i.dataset.valid]
            i.value = i.value.trim()
            
            if( pattern.test( i.value ) ) {
                i.classList.add('is-valid')
            } else {
                i.classList.add('is-invalid')
                isValid = false
            }
        })
        
        if (isValid) {
            const formData = new FormData(signUpForm)
            formData.append('action', 'registration')
            formData.append('nonce', window.bxVars.nonce)
    
            const response = await fetch(window.bxVars.ajaxurl, {
                method: 'POST',
                body: formData
            })
            if( response.ok ) {
                signUpModal.hide()
                let res = await response.text()
                alert('Вы успешно зарегистрировались! ', res)
            }
        }

        e.target.disabled = false
    })

    signInSubmit.addEventListener('click', async e => {
        e.preventDefault()
        e.target.disabled = true

        // Validation 
        isValid = true
        inputs2.forEach(i => {
            let pattern = patterns[i.dataset.valid]
            i.value = i.value.trim()
            
            if( pattern.test( i.value ) ) {
                i.classList.add('is-valid')
            } else {
                i.classList.add('is-invalid')
                isValid = false
            }
        })
        
        if (isValid) {
            const formData = new FormData(signInForm)
            formData.append('action', 'auth')
            formData.append('nonce', window.bxVars.nonce)
    
            const response = await fetch(window.bxVars.ajaxurl, {
                method: 'POST',
                body: formData
            })
            if( response.ok ) {
                signInModal.hide()
                window.location.reload()
            } else {
                alert(`Error!`)
            }
        }

        e.target.disabled = false
    })
})
