const buyModal = new bootstrap.Modal(document.getElementById('buyModal'), {
    keyboard: false
})

const buyModalPhoto = document.getElementById('buyModalPhoto')
const buyModalName = document.getElementById('buyModalName')
const buyModalPrice = document.getElementById('buyModalPrice')
const buyModalCount = document.getElementById('buyModalCount')
const buyModalCost = document.getElementById('buyModalCost')

const productList = document.getElementById('productList')

const addToCart = document.getElementById('addToCart')
const totalCount = document.querySelector('.total-count')
const totalCountLabel = document.querySelector('.total-count-label')
const totalCost  = document.querySelector('.total-cost')

productList.addEventListener('click', event => {
    event.stopPropagation()

    let currentDataset = event.target.dataset

    if (currentDataset.action == 'buy') {
        buyModalPhoto.setAttribute('src', currentDataset.photo)
        buyModalName.innerText = currentDataset.name
        buyModalPrice.innerText = currentDataset.price
        buyModalCount.value = 1
        buyModalCost.innerText = currentDataset.price * buyModalCount.value
       
        buyModal.show()

        buyModalCount.addEventListener('change', event => {
            buyModalCost.innerText = currentDataset.price * buyModalCount.value     
        })
    }
})
   
addToCart.addEventListener('click', event => {
    event.stopPropagation()
    buyModal.hide()

    totalCount.innerText = parseInt(totalCount.innerText) + parseInt(buyModalCount.value)
    totalCost.innerText  = parseInt(totalCost.innerText)  + parseInt(buyModalCost.innerText)
    totalCountLabel.innerText = totalCount.innerText
})


// searchOptions

const allCategories = document.querySelector('#searchOptions .category-all')

allCategories.addEventListener('change', event => {
    let categoryItems = document.querySelectorAll('#searchOptions .category-item')

    for (let item of categoryItems) {
        item.checked = event.target.checked
    }
})

const categoryOption = document.getElementById('categoryOption')

categoryOption.addEventListener('change', event => {
    if (event.target.classList.contains('category-item')) {
        allCategories.checked = false
    }
})

// producer
const producerAll = document.querySelector('#producerOption .producer-all')
producerAll.addEventListener('change', event => {
    let categoryItems = document.querySelectorAll('#producerOption .producer-item')

    for (let item of categoryItems) {
        item.checked = event.target.checked
    }
})

const producerOption = document.getElementById('producerOption')
producerOption.addEventListener('change', event => {
    if (event.target.classList.contains('producer-item')) {
        producerAll.checked = false
    }
})
